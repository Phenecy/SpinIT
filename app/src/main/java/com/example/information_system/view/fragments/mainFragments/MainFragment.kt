package com.example.information_system.view.fragments.mainFragments

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.information_system.R
import com.example.information_system.model.adapters.MainFragPagerAdapter
import com.google.android.material.tabs.TabLayout

@Suppress("DEPRECATION")
class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val mainFragPagerAdapter = MainFragPagerAdapter(activity!!.supportFragmentManager)
        val mainFragViewPager = view.findViewById<ViewPager>(R.id.main_frag_view_pager)
        mainFragViewPager.adapter = mainFragPagerAdapter
        val mainFragTabLayout = view.findViewById<TabLayout>(R.id.main_frag_tab_layout)
        mainFragTabLayout.tabMode = TabLayout.MODE_SCROLLABLE
        mainFragTabLayout.setSelectedTabIndicatorColor(Color.parseColor("#F44336"))
        mainFragTabLayout.setSelectedTabIndicatorHeight(4)

        mainFragTabLayout.setupWithViewPager(mainFragViewPager)
        return view
    }
}