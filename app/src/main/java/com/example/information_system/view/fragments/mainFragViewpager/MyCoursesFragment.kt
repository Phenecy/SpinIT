package com.example.information_system.view.fragments.mainFragViewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.information_system.R
import com.example.information_system.model.adapters.MyCoursesRecyclerAdapter

class MyCoursesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(container!!.context)
            .inflate(R.layout.fragment_mycourses, container, false)
        val myCoursesRecyclerView: RecyclerView = view.findViewById(R.id.my_courses_recycler)
        val myCoursesList = arrayListOf<Any>("dfhsgjkhgjlkdf","dfhsgjkhgjlkdf","dfhsgjkhgjlkdf","dfhsgjkhgjlkdf")
        val adapter = MyCoursesRecyclerAdapter(myCoursesList)
        myCoursesRecyclerView.adapter = adapter
        myCoursesRecyclerView.layoutManager = LinearLayoutManager(container.context)
        return view
    }
}