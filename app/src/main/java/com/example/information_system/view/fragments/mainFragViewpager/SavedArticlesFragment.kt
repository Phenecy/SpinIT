package com.example.information_system.view.fragments.mainFragViewpager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.information_system.R
import com.example.information_system.model.adapters.MainFragSavedArticlesAdapter

class SavedArticlesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = LayoutInflater.from(container!!.context)
            .inflate(R.layout.fragment_saved_articles, container, false)
        val list = arrayListOf<Any>("ldhfgldkhngl","ldhfgldkhngl","ldhfgldkhngl","ldhfgldkhngl")
        val savedArticlesRecycler: RecyclerView = view.findViewById(R.id.saved_articles_recycler)
        val adapter = MainFragSavedArticlesAdapter(list)
        savedArticlesRecycler.layoutManager = LinearLayoutManager(container.context)
        savedArticlesRecycler.adapter = adapter
        return view
    }
}