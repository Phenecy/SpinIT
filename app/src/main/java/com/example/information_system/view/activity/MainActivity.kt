package com.example.information_system.view.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.information_system.R
import com.example.information_system.model.Global
import com.example.information_system.retrofit.RetrofitFactory
import com.example.information_system.view.fragments.mainFragments.BlogFragment
import com.example.information_system.view.fragments.mainFragments.CourseFragment
import com.example.information_system.view.fragments.mainFragments.MainFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

private const val SPLASH_DURATION = 500

/*private lateinit var navController: NavController
private lateinit var animation: Animation
private lateinit var progressBar: ProgressBar
private lateinit var splashLayout: ConstraintLayout
private lateinit var mainLayout: ConstraintLayout*/

class MainActivity : AppCompatActivity() {


    lateinit var drawerLayout: DrawerLayout
    lateinit var toolbar: androidx.appcompat.widget.Toolbar
    lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    lateinit var navigationView: NavigationView
    lateinit var bottomNavigationView: BottomNavigationView
    lateinit var headerView: View
    lateinit var headerNameTV: TextView
    lateinit var headerIV: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFrag(MainFragment())
        initViews()
        bottomNavListener()
        initDrawer()

        val service =
            RetrofitFactory()
                .makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            Log.e("Bearer", Global.token)
            val response = service.getUserInfo(Global.token)
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()!!
                        headerNameTV.text = data.data.first_name + " " + data.data.last_name
                        Glide.with(this@MainActivity).load(data.data.avatar_url).apply(
                            RequestOptions().centerCrop().circleCrop()
                        ).into(headerIV)
                    } else {
                        Log.e(
                            "responseerr",
                            "${response.errorBody()!!.string()} ${response.code()}"
                        )
                    }
                }

            } catch (err: HttpException) {
                Log.e("Retrofit", "${err.printStackTrace()}")
            }
        }
        /* splashLayout = findViewById(R.id.splash_layout)
         progressBar = findViewById(R.id.splash_progress_bar)
         animation = AnimationUtils.loadAnimation(baseContext, R.anim.rotate)*/
    }

    fun initDrawer() {
        setSupportActionBar(toolbar)
        actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.open_nav_drawer,
            R.string.close_nav_drawer
        )
        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
    }

    fun initViews() {
        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.navigation_view)
        bottomNavigationView = findViewById(R.id.bottom_navigation_bar)
        toolbar = findViewById(R.id.toolbar)
        headerView = navigationView.getHeaderView(0)
        headerNameTV = headerView.findViewById(R.id.name_tv)
        headerIV = headerView.findViewById(R.id.profile_iv)

    }

    fun bottomNavListener() {
        bottomNavigationView.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_main -> {
                    loadFrag(MainFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.nav_courses -> {
                    loadFrag(CourseFragment())
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.nav_blog -> {
                    loadFrag(BlogFragment())
                    return@setOnNavigationItemSelectedListener true

                }
                else -> return@setOnNavigationItemSelectedListener true
            }

        }
    }

    fun loadFrag(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
            .commit()
    }
    /*private fun initFunctionality() {
        splashLayout.postDelayed({
            progressBar.visibility = View.GONE
            splashLayout.visibility = View.GONE
            updateUI()
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation) {}

                override fun onAnimationRepeat(animation: Animation) {}

                override fun onAnimationEnd(animation: Animation) {
                    updateUI()
                }
            })
        }, SPLASH_DURATION.toLong())
    }*/

    /* private fun updateUI() {
         setContentView(R.layout.activity_main)
         //setupNavigationDrawer()
         mainLayout = findViewById(R.id.main_layout)
     }*/

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }
}