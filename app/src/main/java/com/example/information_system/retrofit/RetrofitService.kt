package com.example.information_system.retrofit

import com.example.information_system.model.ResponseData
import com.example.information_system.model.UserInfo
import retrofit2.Response
import retrofit2.http.*

interface RetrofitService {
    @FormUrlEncoded
    @POST("api/auth/vk/mobile")
    suspend fun authVK(@Field("access_token") accessToken: String): retrofit2.Response<ResponseData>

    @GET("api/auth/user")
    suspend fun getUserInfo(
        @Header("Authorization") token: String,
        @Header("Accept") access: String ="application/json"
    ): Response<UserInfo>
}