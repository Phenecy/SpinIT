package com.example.information_system.presenter.signin_presenter

import com.example.information_system.model.adapters.StartAppPagerAdapter

interface ISignInPresenter {
    fun animateViewPager()
    fun initPagerAdapter(): StartAppPagerAdapter
    fun setViewPagerAdapter()
}