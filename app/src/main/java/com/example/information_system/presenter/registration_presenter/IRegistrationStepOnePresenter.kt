package com.example.information_system.presenter.registration_presenter

interface IRegistrationStepOnePresenter {
    fun initFragments()
}