package com.example.information_system.presenter.signin_presenter

import com.example.information_system.model.adapters.StartAppPagerAdapter
import com.example.information_system.view.activity.SignInActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SignInPresenter(val signInActivity: SignInActivity):
    ISignInPresenter {
    override fun animateViewPager() {
        CoroutineScope(Dispatchers.Main).launch {
            while (true) {
                for (i in 0..2) {
                    delay(3000)
                    signInActivity.viewPager.currentItem = i
                }
            }

        }
    }

    override fun initPagerAdapter(): StartAppPagerAdapter {
        val fragmentAdapter =
            StartAppPagerAdapter(
                signInActivity.supportFragmentManager
            )
        return fragmentAdapter
    }

    override fun setViewPagerAdapter() {
        val adapter=initPagerAdapter()
        signInActivity.viewPager.adapter=adapter
    }
}