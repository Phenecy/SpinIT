package com.example.information_system.requests

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.information_system.model.Global
import com.example.information_system.model.ResponseData
import com.example.information_system.retrofit.RetrofitFactory
import com.vk.sdk.VKAccessToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException

class RequestVKAuth {
    fun auth(res: VKAccessToken, context: Context): ResponseData {
        var data: ResponseData = ResponseData("", "")
        val service =
            RetrofitFactory()
                .makeRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            val response = service.authVK(res!!.accessToken.toString())
            try {
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        Log.e("response", response.body().toString())
                        data = response.body()!!
                        Global.token="Bearer "+data.token
                    } else {
                        Toast.makeText(
                            context,
                            "${response.code()}",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }

            } catch (err: HttpException) {
                Log.e("Retrofit", "${err.printStackTrace()}")
            }
        }

        return data
    }
}