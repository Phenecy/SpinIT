package com.example.information_system.model.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.information_system.view.fragments.mainFragViewpager.AchivementsFragment
import com.example.information_system.view.fragments.mainFragViewpager.SavedArticlesFragment
import com.example.information_system.view.fragments.mainFragViewpager.MyCoursesFragment

@Suppress("DEPRECATION")
class MainFragPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return MyCoursesFragment()
            1 -> return SavedArticlesFragment()
            2 -> return AchivementsFragment()
        }
        return MyCoursesFragment()
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            "Мои курсы"
        } else if (position == 1) {
            "Сохраненные статьи"
        } else {
            "Достижения"
        }
    }
}