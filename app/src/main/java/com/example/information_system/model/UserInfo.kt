package com.example.information_system.model

data class UserInfo(val data:Data)
data class Data(val id:Double,val first_name:String,val last_name:String,val email:String,val created_at:String,val updated_at:String,val birth_date:String,val gender:String,val phone_number:String,val avatar_url:String,val courses:String,val achievements:String,val posts:String,val comments:String,val liked:String)