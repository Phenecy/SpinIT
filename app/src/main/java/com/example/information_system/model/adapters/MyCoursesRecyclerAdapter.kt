package com.example.information_system.model.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.information_system.R

class MyCoursesRecyclerAdapter(val list: ArrayList<Any>) :
    RecyclerView.Adapter<MyCoursesRecyclerAdapter.MyCoursesViewHolder>() {
    inner class MyCoursesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val myCoursesIV: ImageView = view.findViewById(R.id.my_course_iv)
        val myCoursesNameTV: TextView = view.findViewById(R.id.my_course_name_tv)
        val myCoursesDescTV: TextView = view.findViewById(R.id.my_course_desc)
        fun bind(position: Int) {
            myCoursesNameTV.text = list[position].toString()
            myCoursesDescTV.text = list[position].toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyCoursesViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.my_courses_item, parent, false)
        return MyCoursesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyCoursesViewHolder, position: Int) {
        holder.bind(position)
    }


}