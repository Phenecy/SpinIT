package com.example.information_system.model.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.information_system.R

class MainFragSavedArticlesAdapter(val list: ArrayList<Any>) :
    RecyclerView.Adapter<MainFragSavedArticlesAdapter.CourseViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainFragSavedArticlesAdapter.CourseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false)
        return CourseViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MainFragSavedArticlesAdapter.CourseViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class CourseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val courseNameTV:TextView=view.findViewById(R.id.course_name_tv)
        val courseDescTV:TextView=view.findViewById(R.id.course_desc_tv)
        fun bind(position: Int){
            courseNameTV.text=list[position].toString()
            courseDescTV.text=list[position].toString()
        }
    }
}